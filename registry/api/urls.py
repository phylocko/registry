from django.urls import path

from . import views

urlpatterns = [
    path('<login>/elements/', views.elements_view),
    path('<login>/elements/<element_name>/', views.element_view),
    path('<login>/elements/<element_name>/history/', views.history_view),
    path('<login>/elements/<element_name>/last/', views.history_last_view),
]
