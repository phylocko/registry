from rest_framework import serializers
from rest_framework.settings import api_settings

import registry.core.models as core_models


class ElementTextSerializer(serializers.ModelSerializer):
    created = serializers.DateTimeField(format=api_settings.DATETIME_FORMAT, required=False)
    updated = serializers.DateTimeField(format=api_settings.DATETIME_FORMAT, required=False)
    comment = serializers.CharField(required=False, allow_blank=True)

    class Meta:
        model = core_models.ElementText
        fields = ('id', 'value', 'comment', 'created', 'updated')
        read_only_fields = ('id', 'created', 'updated')


class ElementNumberSerializer(serializers.ModelSerializer):
    created = serializers.DateTimeField(format=api_settings.DATETIME_FORMAT, required=False)
    updated = serializers.DateTimeField(format=api_settings.DATETIME_FORMAT, required=False)
    comment = serializers.CharField(required=False, allow_blank=True)

    class Meta:
        model = core_models.ElementNumber
        fields = ('id', 'value', 'comment', 'created', 'updated')
        read_only_fields = ('id', 'created', 'updated')


class ElementBoolSerializer(serializers.ModelSerializer):
    created = serializers.DateTimeField(format=api_settings.DATETIME_FORMAT, required=False)
    updated = serializers.DateTimeField(format=api_settings.DATETIME_FORMAT, required=False)
    comment = serializers.CharField(required=False, allow_blank=True)

    class Meta:
        model = core_models.ElementBool
        fields = ('id', 'value', 'comment', 'created', 'updated')
        read_only_fields = ('id', 'created', 'updated')


class ElementGPSSerializer(serializers.ModelSerializer):
    created = serializers.DateTimeField(format=api_settings.DATETIME_FORMAT, required=False)
    updated = serializers.DateTimeField(format=api_settings.DATETIME_FORMAT, required=False)
    comment = serializers.CharField(required=False, allow_blank=True)

    class Meta:
        model = core_models.ElementGPS
        fields = ('id', 'longitude', 'latitude', 'comment', 'created', 'updated')
        read_only_fields = ('id', 'created', 'updated')


class UserElementSerializer(serializers.ModelSerializer):
    created = serializers.DateTimeField(format=api_settings.DATETIME_FORMAT, required=False)
    updated = serializers.DateTimeField(format=api_settings.DATETIME_FORMAT, required=False)
    type = serializers.CharField(required=True)
    description = serializers.CharField(required=False, allow_blank=True)

    class Meta:
        model = core_models.UserElement
        fields = ('id', 'name', 'type', 'description', 'created', 'updated')
        read_only_fields = ('id', 'type', 'created', 'updated')
