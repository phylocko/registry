from django.db import IntegrityError
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser

import registry.core.models as core_models
from .serializers import (ElementBoolSerializer,
                          ElementTextSerializer,
                          ElementNumberSerializer,
                          ElementGPSSerializer,
                          UserElementSerializer)

models = {
    'number': core_models.ElementNumber,
    'text': core_models.ElementText,
    'book': core_models.ElementBool,
    'gps': core_models.ElementGPS,
}
serializers = {
    'number': ElementNumberSerializer,
    'text': ElementTextSerializer,
    'bool': ElementBoolSerializer,
    'gps': ElementGPSSerializer,
}


@csrf_exempt
def history_last_view(request, login, element_name):
    user = get_object_or_404(core_models.User, login=login)
    element = get_object_or_404(core_models.UserElement, user=user, name=element_name)
    model = models[element.type]
    values = model.objects.filter(user_element=element).order_by('created').last()

    if request.method == 'GET':
        using_serializer = serializers[element.type]
        serializer = using_serializer(values, many=False)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'DELETE':
        values.delete()
        return JsonResponse({'message': 'Last value successfully deleted'})


@csrf_exempt
def history_view(request, login, element_name):
    user = get_object_or_404(core_models.User, login=login)
    element = get_object_or_404(core_models.UserElement, user=user, name=element_name)
    model = models[element.type]

    if request.method == 'GET':
        values = model.objects.filter(user_element=element)
        using_serializer = serializers[element.type]
        serializer = using_serializer(values, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        request_data = JSONParser().parse(request)
        using_serializer = serializers[element.type]
        serializer = using_serializer(data=request_data)
        if serializer.is_valid():
            instance = model(**serializer.data)
            instance.user_element = element
            instance.save()
            serializer = using_serializer(instance)
            return JsonResponse(serializer.data, status=201)
        else:
            return JsonResponse(serializer.errors, status=400)


@csrf_exempt
def element_view(request, login, element_name):
    user = get_object_or_404(core_models.User, login=login)
    element = get_object_or_404(core_models.UserElement, user=user, name=element_name)
    serializer = UserElementSerializer(element)
    return JsonResponse(serializer.data, safe=False)


@csrf_exempt
def elements_view(request, login):
    user = get_object_or_404(core_models.User, login=login)

    if request.method == 'GET':
        elements = user.elements.all()
        serializer = UserElementSerializer(elements, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        request_data = JSONParser().parse(request)
        serializer = UserElementSerializer(data=request_data)
        if serializer.is_valid():
            instance = core_models.UserElement(**serializer.data)
            instance.user = user
            try:
                instance.save()
            except IntegrityError:
                return JsonResponse({'message': 'The user already has an element '
                                                'with name "%s"' % instance.name},
                                    status=400, )
            serializer = UserElementSerializer(instance)
            return JsonResponse(serializer.data, status=201)
        else:
            return JsonResponse(serializer.errors, status=400)
