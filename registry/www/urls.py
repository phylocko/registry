from django.urls import path

from . import views

urlpatterns = [
    path('', views.home_view, name='home'),
    path('elements/', views.element_list_view, name='element_list'),

    # create elements
    path('elements/create/', views.element_create_view, name='create_element'),

    # edit elements
    path('elements/<element_name>/edit/', views.element_edit_view, name='edit_element'),

    # history
    path('<login>/elements/<element_name>/history/', views.history, name='history'),

    path('login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),
]
