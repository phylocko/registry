from django import forms

import registry.core.models as core_models


class CreateElementForm(forms.ModelForm):
    class Meta:
        model = core_models.UserElement
        fields = ['name', 'type', 'description', 'is_public']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Name'}),
            'type': forms.Select(choices=model.TYPE_CHOICES, attrs={'class': 'form-control'}),
            'description': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Description'}),
        }


class EditElementForm(forms.ModelForm):
    class Meta:
        model = core_models.UserElement
        fields = ['name', 'description', 'is_public']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Name'}),
            'description': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Description'}),
        }


class FilterElementsForm(forms.Form):
    name = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={'class': 'form-control mb-2 mr-2', 'placeholder': 'Name or description'}))

    CHOICES = (('', 'All'),) + core_models.UserElement.TYPE_CHOICES

    type = forms.ChoiceField(required=False, choices=CHOICES,
                             widget=forms.Select(attrs={'class': 'form-control mb-2 mr-sm-2'}))


class ElementTextForm(forms.ModelForm):
    class Meta:
        model = core_models.ElementText
        fields = ['value', 'comment']
        widgets = {
            'value': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Value'}),
            'comment': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Comment'}),
        }


class ElementNumberForm(forms.ModelForm):
    class Meta:
        model = core_models.ElementNumber
        fields = ['value', 'comment']
        widgets = {
            'value': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Value'}),
            'comment': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Comment'}),
        }


class ElementBoolForm(forms.ModelForm):
    class Meta:
        model = core_models.ElementBool
        fields = ['value', 'comment']
        widgets = {
            'value': forms.Select(attrs={'class': 'form-control'}),
            'comment': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Comment'}),
        }


class ElementGPSForm(forms.ModelForm):
    class Meta:
        model = core_models.ElementGPS
        fields = ['latitude', 'longitude', 'comment']
        widgets = {
            'latitude': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Latitude'}),
            'longitude': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Longitude'}),
            'comment': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Comment'}),
        }
