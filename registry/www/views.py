from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.db.models import Q
from django.db import IntegrityError
from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404

from registry.core import models as core_models
from . import forms


@login_required(login_url='login')
def home_view(request):
    return redirect(element_list_view)


@login_required(login_url='login')
def element_list_view(request):
    context = {}

    user_elements = core_models.UserElement.objects.filter(user=request.user)

    form = forms.FilterElementsForm(request.GET or None)
    context['form'] = form

    if form.is_valid():
        search = form.cleaned_data.get('name')
        element_type = form.cleaned_data.get('type')

        if search:
            user_elements = user_elements.filter(Q(name__icontains=search) | Q(description__icontains=search))

        if element_type:
            user_elements = user_elements.filter(type=element_type)

    context['user_elements'] = user_elements

    return render(request, 'elements/element_list.html', context)


@login_required(login_url='login')
def element_create_view(request):
    context = {}
    form = forms.CreateElementForm(request.POST or None)
    if form.is_valid():
        user_element = form.save(commit=False)
        user_element.user = request.user
        try:
            user_element.save()
        except IntegrityError:
            messages.error(request, 'Element with the same name and type already exists!')
            return redirect(element_create_view)

        messages.success(request, 'Element created!')
        return redirect(element_list_view)

    context['form'] = form

    return render(request, 'elements/create_element.html', context)


@login_required(login_url='login')
def element_edit_view(request, element_name):
    context = {}
    element = get_object_or_404(core_models.UserElement, name=element_name, user=request.user)
    context['element'] = element

    form = forms.EditElementForm(instance=element)

    if request.POST:
        action = request.POST.get('action')

        if action == 'update':
            form = forms.EditElementForm(request.POST, instance=element)
            if form.is_valid():
                form.save()
                messages.success(request, 'Element updated!')
                return redirect(element_edit_view, element_name=element.name)

        elif action == 'delete':
            element.delete()
            messages.success(request, 'Element deleted!')
            return redirect(element_list_view)

    context['form'] = form

    return render(request, 'elements/edit_element.html', context)


@login_required(login_url='login')
def history(request, login, element_name):

    context = {}

    if request.user.login == login:
        element = get_object_or_404(core_models.UserElement, name=element_name, user=request.user)
    else:
        element = get_object_or_404(core_models.UserElement, name=element_name, user__login=login, is_public=True)

    models = {
        'number': core_models.ElementNumber,
        'text': core_models.ElementText,
        'bool': core_models.ElementBool,
        'gps': core_models.ElementGPS,
    }

    model = models.get(element.type)
    if not model:
        raise Http404('Unknown Element Type')

    all_history = model.objects.filter(user_element=element).order_by('-created')
    context['element'] = element

    paginator = Paginator(all_history, 30)
    page = request.GET.get('page', 1)
    context['page_objects'] = paginator.get_page(page)
    context['paginator'] = paginator

    using_forms = {
        'number': forms.ElementNumberForm,
        'text': forms.ElementTextForm,
        'bool': forms.ElementBoolForm,
        'gps': forms.ElementGPSForm,
    }

    using_form = using_forms.get(element.type)
    context['form'] = using_form()

    if request.POST:
        action = request.POST.get('action')

        if action == 'add_value':
            using_form = using_forms.get(element.type)
            form = using_form(request.POST)
            if form.is_valid():
                form.instance.user_element = element
                form.save()
                messages.success(request, 'Value added')
                return redirect(history, request.user, element.name)


    return render(request, 'elements/history.html', context)


def login_view(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect(home_view)
        return redirect(login_view)
    return render(request, 'login.html')


def logout_view(request):
    logout(request)
    return redirect(login_view)
