# Generated by Django 2.1.2 on 2018-10-04 16:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='elementnumber',
            old_name='user',
            new_name='user_element',
        ),
    ]
