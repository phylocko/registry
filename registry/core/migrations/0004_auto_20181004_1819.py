# Generated by Django 2.1.2 on 2018-10-04 18:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20181004_1737'),
    ]

    operations = [
        migrations.RenameField(
            model_name='elementbool',
            old_name='user',
            new_name='user_element',
        ),
    ]
