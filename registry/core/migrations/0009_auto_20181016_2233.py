# Generated by Django 2.1.2 on 2018-10-16 22:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_auto_20181009_1642'),
    ]

    operations = [
        migrations.CreateModel(
            name='ElementGPS',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('longitude', models.BooleanField()),
                ('latitude', models.BooleanField()),
                ('comment', models.TextField(blank=True, null=True)),
            ],
            options={
                'db_table': 'core_element_gps',
                'ordering': ['-created'],
            },
        ),
        migrations.AddField(
            model_name='elementbool',
            name='comment',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='elementnumber',
            name='comment',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='elementtext',
            name='comment',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='userelement',
            name='comment',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='elementgps',
            name='user_element',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='gps_values', to='core.UserElement'),
        ),
    ]
