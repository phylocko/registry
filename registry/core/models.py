from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser

from django.core import validators
from django.db import models


class CreatedUpdated(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class UserManager(BaseUserManager):
    use_in_migrations = True


class User(AbstractBaseUser, CreatedUpdated):
    """
    Our custom user
    """
    login = models.CharField(max_length=30, unique=True, null=True, validators=[validators.validate_slug])
    first_name = models.CharField(max_length=30, blank=True, null=True)
    last_name = models.CharField(max_length=30, blank=True, null=True)
    email = models.EmailField(null=True, blank=True, unique=True)

    USERNAME_FIELD = 'login'
    objects = UserManager()

    class Meta:
        db_table = 'core_user'
        managed = True


class UserElement(CreatedUpdated):
    TYPE_CHOICES = (
        ('number', 'Number'),
        ('text', 'Text'),
        ('bool', 'Bool'),
        ('gps', 'GPS'),
    )
    name = models.CharField(max_length=30, null=False, blank=False, validators=[validators.validate_slug])
    type = models.CharField(max_length=30, choices=TYPE_CHOICES, null=False, blank=False)
    description = models.CharField(max_length=128, null=True, blank=True)
    user = models.ForeignKey('User', on_delete=models.CASCADE, related_name='elements')
    is_public = models.BooleanField(default=False)

    class Meta:
        db_table = 'core_user_elements'
        managed = True
        unique_together = ['user', 'name']


class ElementNumber(CreatedUpdated):
    """
    Stores Numeric values
    """
    value = models.FloatField(blank=False, default=0)
    comment = models.TextField(null=True, blank=True)
    user_element = models.ForeignKey('UserElement', on_delete=models.CASCADE, related_name='number_values')

    class Meta:
        db_table = 'core_element_number'
        ordering = ['-created']

    def data(self):
        return {
            'value': self.value,
        }



class ElementText(CreatedUpdated):
    """
    Stores Textual values
    """
    value = models.TextField(null=True, blank=True, default='')
    comment = models.TextField(null=True, blank=True)
    user_element = models.ForeignKey('UserElement', on_delete=models.CASCADE, related_name='text_values')

    class Meta:
        db_table = 'core_element_text'
        ordering = ['-created']

    def data(self):
        return {
            'value': self.value,
        }


class ElementBool(CreatedUpdated):
    """
    Stores Boolean values
    """
    value = models.BooleanField(null=False, blank=False)
    comment = models.TextField(null=True, blank=True)
    user_element = models.ForeignKey('UserElement', on_delete=models.CASCADE, related_name='bool_values')

    class Meta:
        db_table = 'core_element_bool'
        ordering = ['-created']

    def data(self):
        return {
            'value': self.value,
        }


class ElementGPS(CreatedUpdated):
    """
    Stores Longitude and Latitude
    """
    longitude = models.CharField(max_length=50, null=False, blank=False)
    latitude = models.CharField(max_length=50, null=False, blank=False)
    comment = models.TextField(null=True, blank=True)
    user_element = models.ForeignKey('UserElement', on_delete=models.CASCADE, related_name='gps_values')

    class Meta:
        db_table = 'core_element_gps'
        ordering = ['-created']

    def data(self):
        return {
            'longitude': self.longitude,
            'latitude': self.latitude,
        }
