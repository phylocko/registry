FROM python:3.6-alpine
RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app/
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev
COPY . .
RUN pip install -r requirements.txt
CMD ["gunicorn", "registry.wsgi:application", "--bind=0.0.0.0:8000"]
